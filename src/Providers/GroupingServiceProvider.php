<?php

namespace MVCommerce\Grouping\Providers;


use Illuminate\Support\ServiceProvider;

class GroupingServiceProvider extends ServiceProvider
{


    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['mvcommerce', 'mvcommerce_product', 'mvcommerce_product_migrations', 'migrations']);


    }


    public function register()
    {

        // Bind Facades -----------------------
        $this->app->bind('mvcommerce.grouping', Groups::class);

    }

}
