<?php

namespace MVCommerce\Grouping\Facades;


use Illuminate\Support\Facades\Facade;

class Groups extends Facade
{


    protected static function getFacadeAccessor()
    {
        return 'mvcommerce.grouping';
    }

}
