<?php

namespace MVCommerce\Grouping\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use MVCommerce\Grouping\Traits\HasGroups;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 *
 * @property string $name
 * @property string $type
 * @property string $description
 * @property Group[]|Collection $children
 * @property Group[]|Collection $parents
 */
class Group extends Model
{

    use SoftDeletes, HasGroups;


    protected $fillable = [
        'name', 'type', 'description'
    ];



    public function children(){
        return $this->objects(static::class);
    }


    public function parents(){
        return $this->groups();
    }


    /**
     * Get Objects of a specific type.
     *
     * @param string $groupable A class that represents groupable types.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function objects($groupable){

        // We can't directly use any static class here.

        return $this->morphedByMany($groupable, 'groupable', 'group_object');
    }


    /**
     * @param Builder $query
     * @param string|array $types
     * @return Builder
     */
    public function scopeTypes(Builder $query, $types){
        $types = Arr::wrap($types);
        return $query->whereIn('type', $types);
    }


    /**
     * @param Builder $query
     * @param string|array $types
     * @return Builder
     */
    public function scopeType(Builder $query, $types){
        return $this->scopeTypes($query, $types);
    }


}
