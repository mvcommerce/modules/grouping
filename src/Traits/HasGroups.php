<?php

namespace MVCommerce\Grouping\Traits;

use Illuminate\Database\Eloquent\Collection;
use MVCommerce\Grouping\Models\Group;


/**
 * Trait HasGroups
 * @package MVCommerce\Grouping\Traits
 *
 * @property Group[]|Collection $groups
 */
trait HasGroups
{


    public function groups(){
        return $this->morphToMany(Group::class, 'groupable', 'group_object');
    }


}
